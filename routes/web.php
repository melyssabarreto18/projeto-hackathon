<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProfessoresController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('/home', [ProfessoresController::class, 'index'])->name('professor.home');
Route::get('/index', [ProfessoresController::class, 'show'])->name('professor.show');
Route::get('/create', [ProfessoresController::class, 'create'])->name('professor.create');
Route::get('/edit', [ProfessoresController::class, 'edit'])->name('professor.edit');
// Route::get('/destroy', [ProfessoresController::class, 'destroy'])->name('professor.destroy');

require __DIR__.'/auth.php';

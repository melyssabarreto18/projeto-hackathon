<h3>TELA DE CADASTRO DE CURSOS</h3>

<div class="p-6 mb-4">
    <form method="GET" action="{{ route('curso.create') }}" enctype="multipart/form-data" id="form-cad">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nome do curso</label>
                        <input name="nome" type="text" class="form-control" placeholder="Digite o nome do curso" value="" required="">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Modalidade</label>
                        <select name="modalidade" class="form-select" required>
                            <option selected disabled value="">Selecione uma modalidade</option>
                            @foreach($modalidades as $modalidade)
                                <option value="{{ $modalidade->id }}">{{ $modalidade->nome }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>          

                <div class="col-md-3">
                    <div class="form-group">
                        <label>Turno</label>
                        <select name="turno" class="form-select" required>
                            <option selected disabled value="">Selecione um turno</option>
                            @foreach($turnos as $turno)
                                <option value="{{ $turno->id }}">{{ $turno->tipo }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>          

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Descrição</label>
                        <textarea 
                            name="descricao" type="text" class="form-control" placeholder="Digite uma descrição" value="" required="">
                        </textarea>
                    </div>
                </div>  
            </div>
        </div>
        <footer class="card-footer" style="display: flex; justify-content: end;">
            <button type="submit" id="botaoEnviar" class="btn btn-success">
                <i class="mr-1">
                    <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z">
                        </path>
                    </svg>
                </i>
                Salvar
            </button>
        </footer>
    </form>
</div>
    
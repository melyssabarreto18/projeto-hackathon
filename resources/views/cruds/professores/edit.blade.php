<h3>TELA DE EDIÇÃO DE PROFESSOR</h3>
<div class="card auto-fit mx-5">
    <div class="card-header" style="background-color: rgb(49, 89, 228);">
        <h3 class="card-title" style="color: rgb(255, 255, 255); margin: 5px 5px 5px; font-size: 20px;">Edição Cadastro</h3>
    </div>

    <form method="POST" action="{{ route('professor.update', ['id' => $professor->id]) }}" onsubmit="desabilitarBotao()">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Nome Completo</label>
                        <input name="nome" type="text" class="form-control" value="{{ $professor->nome }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>CPF</label>
                        <input name="cpf" maxlength="14" type="text" class="form-control" value="{{ $professor->cpf }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Matrícula</label>
                        <input name="matricula" maxlength="15" type="text" class="form-control" value="{{ $professor->matricula }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>E-mail</label>
                        <input name="email" type="email" class="form-control" value="{{ $professor->email }}">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Telefone</label>
                        <input name="telefone" type="text" maxlength="15" class="form-control" value="{{ $professor->telefone }}">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Cargo</label>
                        <select name="cargo" class="form-select" required value="">
                            @foreach($cargos as $cargo)
                                <option @selected($professor->fk_cargo == $cargo->id) value="{{ $cargo->id }}">{{ $cargo->tipo }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>         
        </div>
        <footer class="card-footer" style="display: flex; justify-content: end;">
            <button type="submit" id="botaoEnviar" class="btn btn-success">
                <i>
                    <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z">
                        </path>
                    </svg>
                </i> Salvar
            </button>
        </footer>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#cpf').on('input', function() {
            var cpf = $(this).val();
            cpf = cpf.replace(/\D/g, ''); 
            cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2'); 
            cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2'); 
            cpf = cpf.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); 
            $(this).val(cpf);
        });

        
        $('#telefone').on('input', function() {
            var telefone = $(this).val();
            telefone = telefone.replace(/\D/g, ''); 
            telefone = telefone.replace(/^(\d{2})(\d)/g, '($1) $2'); 
            telefone = telefone.replace(/(\d)(\d{4})$/, '$1-$2'); 
            $(this).val(telefone);
        });
    });
</script>

<h3>TELA DE CADASTRO DE DISCIPLINAS</h3>

<div class="p-6 mb-4">
    <form method="GET" action="{{ route('disciplina.create') }}" enctype="multipart/form-data" id="form-cad">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Nome da disciplina</label>
                        <input name="nome" type="text" class="form-control" placeholder="Digite o nome da disciplina" value="" required="">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Carga Horária Semanal</label>
                        <input name="cargaHoraria_semanal" type="text" maxlength="1"  class="form-control" placeholder="Digite o a carga horária semanal" required="">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Eixo Temático</label>
                        <select name="eixo" class="form-select" required>
                            <option selected disabled value="">Selecione um eixo temático</option>
                            @foreach($eixos as $eixo)
                                <option value="{{ $eixo->id }}">{{ $eixo->nome }}</option>
                            @endforeach
                        </select>                     
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Professor Responsável</label>
                        <select name="professor" class="form-select">
                            <option selected disabled value="">Selecione um professor</option>
                            @foreach($professores as $professor)
                                <option value="{{ $professor->id }}">{{ $professor->nome }}</option>
                            @endforeach
                        </select>                    
                    </div>
                </div>                      
            </div>
        </div>
        <footer class="card-footer" style="display: flex; justify-content: end;">
            <button type="submit" id="botaoEnviar" class="btn btn-success">
                <i class="mr-1">
                    <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z">
                        </path>
                    </svg>
                </i>
                Salvar
            </button>
        </footer>
    </form>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#cpf').on('input', function() {
            var cpf = $(this).val();
            cpf = cpf.replace(/\D/g, ''); 
            cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2'); 
            cpf = cpf.replace(/(\d{3})(\d)/, '$1.$2'); 
            cpf = cpf.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); 
            $(this).val(cpf);
        });

        
        $('#telefone').on('input', function() {
            var telefone = $(this).val();
            telefone = telefone.replace(/\D/g, ''); 
            telefone = telefone.replace(/^(\d{2})(\d)/g, '($1) $2'); 
            telefone = telefone.replace(/(\d)(\d{4})$/, '$1-$2'); 
            $(this).val(telefone);
        });
    });
</script>
    
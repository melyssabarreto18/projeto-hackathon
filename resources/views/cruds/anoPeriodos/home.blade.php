<h3>TELA DE ANOS E PERÍODOS</h3>

<div class="card auto-fit" style="margin: 20px 45px 25px;">
    <div class="card-header" style="background-color: rgb(49, 89, 228);">
        <h3 class="card-title" style="color: rgb(255, 255, 255); margin: 5px 5px 5px; font-size: 20px;">Anos e Períodos</h3>
    </div>

    <div class="d-flex justify-content-end px-3 mt-2 mb-0">
        <span class="pull-right">
            <a class="btn btn-primary buttonResponsive m-0" href="{{ route('anoPeriodo.show') }}" id="btnVincular" name="btnVincular" role="button" data-bs-toggle="tooltip" data-bs-placement="top">
                <i class="ace-icon fa fa-plus"></i>Cadastrar Ano
            </a>
        </span>
    </div>

    <div class="d-flex justify-content-end px-3 mt-2 mb-0">
        <span class="pull-right">
            <a class="btn btn-primary buttonResponsive m-0" href="{{ route('anoPeriodo.show') }}" id="btnVincular" name="btnVincular" role="button" data-bs-toggle="tooltip" data-bs-placement="top">
                <i class="ace-icon fa fa-plus"></i>Cadastrar Período
            </a>
        </span>
    </div>

    <div class="card-body">
        <div class="row m-0">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 4%; border-left: 1px solid #bcbec2; border-right: 1px solid #bcbec2; text-align: center;">#</th>
                            <th style="width: auto; border-left: 1px solid #bcbec2; border-right: 1px solid #bcbec2; text-align: center;">Anos</th>
                            <th style="width: auto; border-left: 1px solid #bcbec2; border-right: 1px solid #bcbec2; text-align: center;">Períodos</th>
                            <th style="width: auto; border-left: 1px solid #bcbec2; border-right: 1px solid #bcbec2; text-align: center;">Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($anoPeriodos as $ap)
                            <tr>
                                <td align="center" style="font-size: 14px; border-right: 1px solid #bcbec2; border-left: 1px solid #bcbec2">{{ $ap->id }}</td>
                                <td align="center" style="font-size: 14px; border-right: 1px solid #bcbec2; border-left: 1px solid #bcbec2">{{ $ap->ano }}</td>
                                <td align="center" style="font-size: 14px; border-right: 1px solid #bcbec2; border-left: 1px solid #bcbec2">{{ $ap->periodo }}</td>
                                <td>
                                    <a class="card btn btn-xs bg-success d-flex flex-column justify-content-center align-items-center" style="width: 150px; height: 100px; margin-right: 10px;" href="{{ route('anoPeriodo.edit', ['id' => $ap->id]) }}" title="Editar" name="btnEditar" role="button" data-bs-toggle="tooltip" data-bs-placement="top">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#fff" viewBox="0 0 256 256">
                                            <path
                                                d="M227.32,73.37,182.63,28.69a16,16,0,0,0-22.63,0L36.69,152A15.86,15.86,0,0,0,32,163.31V208a16,16,0,0,0,16,16H92.69A15.86,15.86,0,0,0,104,219.31l83.67-83.66,3.48,13.9-36.8,36.79a8,8,0,0,0,11.31,11.32l40-40a8,8,0,0,0,2.11-7.6l-6.9-27.61L227.32,96A16,16,0,0,0,227.32,73.37ZM48,179.31,76.69,208H48Zm48,25.38L51.31,160,136,75.31,180.69,120Zm96-96L147.32,64l24-24L216,84.69Z">
                                            </path>
                                        </svg>
                                        <p>Editar</p>
                                    </a>
                                    <a class="card btn btn-xs bg-success d-flex flex-column justify-content-center align-items-center" style="width: 150px; height: 100px; margin-right: 10px;" href="{{ route('anoPeriodo.destroy', ['id' => $ap->id]) }}" title="Excluir" name="btnExcluir" role="button" data-bs-toggle="tooltip" data-bs-placement="top">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#fff" viewBox="0 0 256 256">
                                            <path
                                                d="M227.32,73.37,182.63,28.69a16,16,0,0,0-22.63,0L36.69,152A15.86,15.86,0,0,0,32,163.31V208a16,16,0,0,0,16,16H92.69A15.86,15.86,0,0,0,104,219.31l83.67-83.66,3.48,13.9-36.8,36.79a8,8,0,0,0,11.31,11.32l40-40a8,8,0,0,0,2.11-7.6l-6.9-27.61L227.32,96A16,16,0,0,0,227.32,73.37ZM48,179.31,76.69,208H48Zm48,25.38L51.31,160,136,75.31,180.69,120Zm96-96L147.32,64l24-24L216,84.69Z">
                                            </path>
                                        </svg>
                                        <p>Excluir</p>
                                    </a>                
                                    {{-- <button type="button" class="card btn btn-xs bg-danger d-flex flex-column justify-content-center align-items-center" style="width: 150px; height: 100px; margin-right: 10px;" title="Excluir" data-bs-toggle="modal" data-bs-target="#confirmDeleteModal">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="#fff" viewBox="0 0 256 256">
                                            <path
                                                d="M216,48H176V40a24,24,0,0,0-24-24H104A24,24,0,0,0,80,40v8H40a8,8,0,0,0,0,16h8V208a16,16,0,0,0,16,16H192a16,16,0,0,0,16-16V64h8a8,8,0,0,0,0-16ZM96,40a8,8,0,0,1,8-8h48a8,8,0,0,1,8,8v8H96Zm96,168H64V64H192ZM112,104v64a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Zm48,0v64a8,8,0,0,1-16,0V104a8,8,0,0,1,16,0Z">
                                            </path>
                                        </svg>
                                        <p>Excluir</p>
                                    </button>--}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="12">
                                    <div class="well text-center "><br>
                                        <h2 class="text-danger"><i class="fa fa-warning"></i> Nenhum Registro Encontrado!</h2>
                                    </div>
                                </td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    {{-- <div class="modal fade" id="confirmDeleteModal" tabindex="-1" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="confirmDeleteModalLabel">Confirmação</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Fechar"></button>
                </div>
                <div class="modal-body">Tem certeza de que deseja excluir este professor?</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Cancelar</button>
                    <form id="deleteForm" action="{{ route('professor.destroy', ['id' => $professor->id]) }}" method="POST">
                      @csrf
                      @method('DELETE')
                      <a class="btn btn-danger" href="javascript:void();" onclick="return document.getElementById('deleteForm').submit();">Excluir</a>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
</div>

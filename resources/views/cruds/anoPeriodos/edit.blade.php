<h3>TELA DE EDIÇÃO DE TURNO</h3>
<div class="card auto-fit mx-5">
    <div class="card-header" style="background-color: rgb(49, 89, 228);">
        <h3 class="card-title" style="color: rgb(255, 255, 255); margin: 5px 5px 5px; font-size: 20px;">Edição Cadastro</h3>
    </div>

    <form method="POST" action="{{ route('turno.update', ['id' => $turno->id]) }}" onsubmit="desabilitarBotao()">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Turno</label>
                        <input name="tipo" type="text" class="form-control" value="{{ $turno->tipo }}" required>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label>Horário de Início</label>
                        <input name="horario_inicio" type="time" class="form-control" value="{{ $turno->horario_inicio }}" required>
                    </div>
                </div>
            
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Horário de Término</label>
                        <input name="horario_fim" type="time" class="form-control" value="{{ $turno->horario_fim }}" required>
                    </div>
                </div>
            </div>
        </div>
        <footer class="card-footer" style="display: flex; justify-content: end;">
            <button type="submit" id="botaoEnviar" class="btn btn-success">
                <i>
                    <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16" height="15" width="15" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z">
                        </path>
                    </svg>
                </i> Salvar
            </button>
        </footer>
    </form>
</div>

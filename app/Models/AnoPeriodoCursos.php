<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Mpdf\Tag\Select;

class AnoPeriodoCursos extends Model
{
    use HasFactory;

    protected $table = "ano_periodo_cursos";

    protected $fillable = [
        'fk_ano',
        'fk_periodo',
        'fk_cursos'
    ];
}



<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Mpdf\Tag\Select;

class DisciplinasCursos extends Model
{
    use HasFactory;

    protected $table = "disciplinas_cursos";

    protected $fillable = [
        'fk_disciplinas',
        'fk_cursos'
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use Mpdf\Tag\Select;

class Professores extends Model
{
  use HasFactory;

  protected $table = "professores";

  protected $fillable = [
    'nome',
    'cpf',
    'matricula',
    'email',
    'telefone',
    'fk_cargo'
  ];
}

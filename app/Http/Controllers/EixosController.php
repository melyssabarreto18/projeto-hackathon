<?php

namespace App\Http\Controllers;

use App\Models\Eixos;
use Illuminate\Http\Request;

class EixosController extends Controller
{
    public function index(Request $request) 
    {
        $eixos = Eixos::all(); 

        return view('cruds.eixos.home', [
            'eixos' => $eixos
        ]);
    }

    public function show(Request $request) 
    {
        return view('cruds.eixos.create');
    }

    public function create(Request $request)
    {
        try {
            $eixoExistente = Eixos::where('nome', $request->nome)->first();
            
            if ($eixoExistente) {
                return redirect()->route('eixo.home')->withErrors('Já existe um eixo com esse nome!');
            }

            $eixo = new Eixos([
                'nome' => $request->nome,
                'descricao' => $request->descricao
            ]);

            $eixo->save();

            return redirect()->route('eixo.home')->with('success', 'Eixo cadastrado com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao cadastrar eixo!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $eixo = Eixos::findOrFail($id);

            return view('cruds.eixos.edit', [
                'eixo' => $eixo
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados do eixo!');
        }
    }

    public function update($id, Request $request)
    {
        try {
            $eixoOriginal = Eixos::find($id);

            if ($request->nome !== $eixoOriginal->nome) {
                $eixoExistente = Eixos::where('nome', $request->nome)->first();
                if ($eixoExistente) {
                    return redirect()->route('eixo.home')->withErrors('Já existe um eixo com esse nome!');
                }
            }

            $eixoOriginal->update([
                'nome' => $request->input('nome'),
                'descricao' => $request->input('descricao'),
            ]);

            return redirect()->route('eixo.home', ['id' => $id])->with('success', 'Eixo atualizado com sucesso!');
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao atualizar o eixo!');
        }
    }

    public function destroy($id, Request $request)
    {
      try {
        Eixos::destroy((int)$id);

        return redirect()->route('eixo.home')->with('success', 'Eixo deletado com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar o eixo!');
      }
    }  
}

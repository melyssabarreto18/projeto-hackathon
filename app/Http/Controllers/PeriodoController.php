<?php

namespace App\Http\Controllers;

use App\Models\AnoPeriodo;
use Illuminate\Http\Request;

class AnoPeriodoController extends Controller
{
    public function index(Request $request) 
    {
        $anoPeriodos = AnoPeriodo::all(); 

        return view('cruds.anoPeriodos.home', [
            'anoPeriodos' => $anoPeriodos
        ]);
    }

    public function show(Request $request) 
    {
        return view('cruds.anoPeriodos.create');
    }

    public function create(Request $request)
    {
        try {
            $anoExistente = AnoPeriodo::where('ano', $request->ano)->first();
            $periodoExistente = AnoPeriodo::where('periodo', $request->periodo)->first();

            if ($anoExistente) {
                return redirect()->route('anoPeriodo.home')->withErrors('Já existe um ano com esse valor!');
            }

            if ($periodoExistente) {
                return redirect()->route('anoPeriodo.home')->withErrors('Já existe um período com esse valor!');
            }

            $anoPeriodo = new AnoPeriodo([
                'ano' => $request->ano,
                'periodo' => $request->periodo
            ]);

            $anoPeriodo->save();

            return redirect()->route('anoPeriodo.home')->with('success', 'Cadastrado efetuado com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha no cadastro!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $turno = Turnos::findOrFail($id);

            return view('cruds.turnos.edit', [
                'turno' => $turno
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados do turno!');
        }
    }

    public function update($id, Request $request)
    {
        try {
            $turnoOriginal = Turnos::find($id);

            if ($request->tipo !== $turnoOriginal->tipo) {
                $turnoExistente = Turnos::where('tipo', $request->tipo)->first();
                if ($turnoExistente) {
                    return redirect()->route('turno.home')->withErrors('Já existe um turno com esse nome!');
                }
            }

            $turnoOriginal->update([
                'tipo' => $request->input('tipo'),
                'horario_inicio' => $request->input('horario_inicio'),
                'horario_fim' => $request->input('horario_fim')
            ]);

            return redirect()->route('turno.home', ['id' => $id])->with('success', 'Turno atualizado com sucesso!');
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao atualizar o turno!');
        }
    }

    public function destroy($id, Request $request)
    {
      try {
        Turnos::destroy((int)$id);

        return redirect()->route('turno.home')->with('success', 'Turno deletado com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar o turno!');
      }
    }  
}

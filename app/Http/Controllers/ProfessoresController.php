<?php

namespace App\Http\Controllers;

use App\Models\Apenado\Apenado;
use App\Models\Assistencias\Atendimentos\Atendimentos;
use App\Models\Assistencias\Questionarios\QuestionarioArea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use App\Models\Unidades\Unidades;
use Exception;

class ProfessoresController extends Controller
{
    public function index(Request $request)
    {
        return view('professores.index');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Modalidades;
use Illuminate\Http\Request;

class ModalidadesController extends Controller
{
    public function index(Request $request) 
    {
        $modalidades = Modalidades::all(); 

        return view('cruds.modalidades.home', [
            'modalidades' => $modalidades
        ]);
    }

    public function show(Request $request) 
    {
        return view('cruds.modalidades.create');
    }

    public function create(Request $request)
    {
        try {
            $modalidadeExistente = Modalidades::where('nome', $request->nome)->first();
            
            if ($modalidadeExistente) {
                return redirect()->route('modalidade.home')->withErrors('Já existe uma modalidade com esse nome!');
            }

            $modalidade = new Modalidades([
                'nome' => $request->nome,
            ]);

            $modalidade->save();

            return redirect()->route('modalidade.home')->with('success', 'Modalidade cadastrada com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao cadastrar modalidade!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $modalidade = Modalidades::findOrFail($id);

            return view('cruds.modalidades.edit', [
                'modalidade' => $modalidade
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados da modalidade!');
        }
    }

    public function update($id, Request $request)
    {
        try {
            $modalidadeOriginal = Modalidades::find($id);

            if ($request->nome !== $modalidadeOriginal->nome) {
                $modalidadeExistente = Modalidades::where('nome', $request->nome)->first();
                if ($modalidadeExistente) {
                    return redirect()->route('modalidade.home')->withErrors('Já existe uma modalidade com esse nome!');
                }
            }

            $modalidadeOriginal->update([
                'nome' => $request->input('nome')
            ]);

            return redirect()->route('modalidade.home', ['id' => $id])->with('success', 'Modalidade atualizada com sucesso!');
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao atualizar a modalidade!');
        }
    }

    public function destroy($id, Request $request)
    {
      try {
        Modalidades::destroy((int)$id);

        return redirect()->route('modalidade.home')->with('success', 'Modalidade deletada com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar a modalidade!');
      }
    }  
}

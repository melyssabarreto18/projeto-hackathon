<?php

namespace App\Http\Controllers;

use App\Models\Cursos;
use App\Models\Modalidades;
use App\Models\Turnos;
use Illuminate\Http\Request;

class CursosController extends Controller
{
    public function index(Request $request) 
    {
        $cursos = Cursos::all(); 
        $modalidades = Modalidades::pluck('nome', 'id'); 
        $turnos = Turnos::pluck('tipo', 'id'); 

        return view('cruds.cursos.home', [
            'cursos' => $cursos,
            'modalidades' => $modalidades,
            'turnos' => $turnos
        ]);
    }

    public function show(Request $request) 
    {
        $modalidades = Modalidades::all(); 
        $turnos = Turnos::all(); 

        return view('cruds.cursos.create', [
            'modalidades' => $modalidades,
            'turnos' => $turnos
        ]);
    }
    
    public function create(Request $request)
    {
        try {
            $cursoExistente = Cursos::where('nome', $request->nome)->first();

            if ($cursoExistente) {
                return redirect()->route('curso.home')->withErrors('Já existe uma curso com esse nome!');
            }

            $curso = new Cursos([
                'nome' => $request->nome,
                'descricao' => $request->descricao,
                'fk_modalidade' => $request->modalidade,
                'fk_turno' => $request->turno,
            ]);

            $curso->save();

            return redirect()->route('curso.home')->with('success', 'Curso cadastrado com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao cadastrar o curso!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $curso = Cursos::findOrFail($id);
            $modalidades = Modalidades::all(); 
            $turnos = Turnos::all();

            return view('cruds.cursos.edit', [
                'curso' => $curso,
                'modalidades' => $modalidades,
                'turnos' => $turnos,
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados do curso!');
        }
    }


    public function update($id, Request $request)
    {
      try {
        $cursoOriginal = Cursos::find($id);
        if ($request->nome !== $cursoOriginal->nome) {
            $cursoExistente = Cursos::where('nome', $request->nome)->first();
            if ($cursoExistente) {
                return redirect()->route('curso.home')->withErrors('Já existe um curso com esse nome!');
            }
        }
        $cursoOriginal->update([
            'nome' => $request->input('nome'),
            'descricao' => $request->input('descricao'),
            'fk_modalidade' => $request->input('modalidade'),
            'fk_turno' => $request->input('turno'),
        ]);

        return redirect()->route('curso.home', ['id' => $id])->with('success', 'Curso atualizado com sucesso!');
    } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao atualizar o curso!');
      }
    }

    public function destroy($id, Request $request)
    {
      try {
        Cursos::destroy((int)$id);

        return redirect()->route('curso.home')->with('success', 'Curso deletado com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar o curso!');
      }
    }
}

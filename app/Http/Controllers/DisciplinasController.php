<?php

namespace App\Http\Controllers;

use App\Models\Disciplinas;
use App\Models\Eixos;
use App\Models\Professores;
use Illuminate\Http\Request;

class DisciplinasController extends Controller
{
    public function index(Request $request) 
    {
        $disciplinas = Disciplinas::all(); 
        $professores = Professores::all(); 
        $eixos = Eixos::pluck('nome', 'id'); 

        return view('cruds.disciplinas.home', [
            'disciplinas' => $disciplinas,
            'eixos' => $eixos,
            'professores' => $professores
        ]);
    }

    public function show(Request $request) 
    {
        $eixos = Eixos::all(); 
        $professores = Professores::all(); 

        return view('cruds.disciplinas.create', [
            'eixos' => $eixos,
            'professores' => $professores
        ]);
    }

    public function create(Request $request)
    {
        try {
            $disciplinaExistente = Disciplinas::where('nome', $request->nome)->first();

            if ($disciplinaExistente) {
                return redirect()->route('disciplina.home')->withErrors('Já existe uma disciplina com esse nome!');
            }

            $disciplina = new Disciplinas([
                'nome' => $request->nome,
                'cargaHoraria_semanal' => $request->cargaHoraria_semanal,
                'fk_professor' => $request->professor,
                'fk_eixo' => $request->eixo,
            ]);

            $disciplina->save();

            return redirect()->route('disciplina.home')->with('success', 'Disciplina cadastrada com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao cadastrar disciplina!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $disciplina = Disciplinas::findOrFail($id);
            $eixos = Eixos::all(); 
            $professores = Professores::all();

            return view('cruds.disciplinas.edit', [
                'disciplina' => $disciplina,
                'professores' => $professores,
                'eixos' => $eixos,
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados da disciplina!');
        }
    }


    public function update($id, Request $request)
    {
      try {
        $disciplinaOriginal = Disciplinas::find($id);
        if ($request->nome !== $disciplinaOriginal->nome) {
            $disciplinaExistente = Disciplinas::where('nome', $request->nome)->first();
            if ($disciplinaExistente) {
                return redirect()->route('disciplina.home')->withErrors('Já existe uma disciplina com esse nome!');
            }
        }
        $disciplinaOriginal->update([
            'nome' => $request->input('nome'),
            'cargaHoraria_semanal' => $request->input('cargaHoraria_semanal'),
            'fk_professor' => $request->input('professor'),
            'fk_eixo' => $request->input('eixo'),
        ]);

        return redirect()->route('disciplina.home', ['id' => $id])->with('success', 'Disciplina atualizada com sucesso!');
    } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao atualizar a disciplina!');
      }
    }

    public function destroy($id, Request $request)
    {
      try {
        Disciplinas::destroy((int)$id);

        return redirect()->route('disciplina.home')->with('success', 'Disciplina deletada com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar a disciplina!');
      }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Turnos;
use Illuminate\Http\Request;

class TurnosController extends Controller
{
    public function index(Request $request) 
    {
        $turnos = Turnos::all(); 

        return view('cruds.turnos.home', [
            'turnos' => $turnos
        ]);
    }

    public function show(Request $request) 
    {
        return view('cruds.turnos.create');
    }

    public function create(Request $request)
    {
        try {
            $turnoExistente = Turnos::where('tipo', $request->tipo)->first();
            
            if ($turnoExistente) {
                return redirect()->route('turno.home')->withErrors('Já existe um turno com esse nome!');
            }

            $turno = new Turnos([
                'tipo' => $request->nome,
                'horario_inicio' => $request->horario_inicio,
                'horario_fim' => $request->horario_fim
            ]);

            $turno->save();

            return redirect()->route('turno.home')->with('success', 'Turno cadastrado com sucesso!');  
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao cadastrar turno!');
        }
    }

    public function edit($id, Request $request) 
    {
        try {
            $turno = Turnos::findOrFail($id);

            return view('cruds.turnos.edit', [
                'turno' => $turno
            ]);
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao carregar os dados do turno!');
        }
    }

    public function update($id, Request $request)
    {
        try {
            $turnoOriginal = Turnos::find($id);

            if ($request->tipo !== $turnoOriginal->tipo) {
                $turnoExistente = Turnos::where('tipo', $request->tipo)->first();
                if ($turnoExistente) {
                    return redirect()->route('turno.home')->withErrors('Já existe um turno com esse nome!');
                }
            }

            $turnoOriginal->update([
                'tipo' => $request->input('tipo'),
                'horario_inicio' => $request->input('horario_inicio'),
                'horario_fim' => $request->input('horario_fim')
            ]);

            return redirect()->route('turno.home', ['id' => $id])->with('success', 'Turno atualizado com sucesso!');
        } catch (Exception $e) {
            Log::error($e);
            return redirect()->back()->withErrors('Falha ao atualizar o turno!');
        }
    }

    public function destroy($id, Request $request)
    {
      try {
        Turnos::destroy((int)$id);

        return redirect()->route('turno.home')->with('success', 'Turno deletado com sucesso!');
      } catch (Exception $e) {
        Log::error($e);
        return redirect()->back()->withErrors('Falha ao deletar o turno!');
      }
    }  
}

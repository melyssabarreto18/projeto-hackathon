<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \App\Models\User::create([
            'name' => 'Gestor',
            'email' => 'gestor@mail.com',
            'cargo' => 'Gestor',
            'telefone' => '(69)98765-4321',
            'password' => bcrypt('123')
        ]);
        \App\Models\User::factory()->count(30)->create();
    }
}

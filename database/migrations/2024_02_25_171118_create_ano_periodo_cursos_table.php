<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ano_periodo_cursos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fk_ano')->constrained('ano');
            $table->foreignId('fk_periodo')->constrained('periodo');
            $table->foreignId('fk_cursos')->constrained('cursos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ano_periodo_cursos');
    }
};

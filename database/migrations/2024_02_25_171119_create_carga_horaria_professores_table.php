<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('carga_horaria_professores', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fk_professor')->constrained('professores');
            $table->foreignId('fk_disciplina')->constrained('disciplinas');
            $table->integer('cargaHoraria_semanal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('carga_horaria_professores');
    }
};
